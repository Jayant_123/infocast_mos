Feature: Testing Infocast MOS Brokerage Scheme page.

@brokerage_scheme@regression
Scenario: Verify fields displayed in Brokerage Scheme Maintenance page.
Given Verify Testcase with testid "IINV_MOS-58"
Given Logon to MOS application with valid user credentials.
Then Click on the System -> Brokerage Scheme page.
When In Brokerage Scheme page, comes with search and result tabs.
Then verify fields in search tab
And verify fields in result tab.


@brokerage_scheme@regression
Scenario: Verify the functionlity of Clear button in the Brokerage Scheme page.
Given Verify Testcase with testid "IINV_MOS-59"
Given Logon to MOS application with valid user credentials.
Then Click on the System -> Brokerage Scheme page.
When In Brokerage Scheme page, enter random values in Search fields.
Then click on clear field.
Then System should retrive back previous values.

@brokerage_scheme@regression
Scenario Outline: Verify and validate different functionality in User Account page.
Given Verify Testcase with testid "<TestcaseID>"
Given Logon to MOS application with valid user credentials.
Then Click on the System -> Brokerage Scheme page.
When In Brokerage Scheme page, enter valid User ID and click on Search button.
When In Brokerage Scheme page,User clicks on the first entry.
When user "<action>"
And  System "<result>"

Examples:

|   TestcaseID  |                          action                                                            |                      result                                                                        |
|  IINV_MOS-60  |                 "modify Scheme Name and click Reload button"				                 |  "should retrieve back the original data in Scheme Name."     								|
|  IINV_MOS-61  |                      "click Delete button "						                         |  "displays prompt window for Yes/No confirmation"     												|
|  IINV_MOS-62  |         "input valid Scheme Name, Rate % as 1 and click Save button"                       |  "should save the data and display Saved message."     												|
|  IINV_MOS-63  |   "input blank value for Scheme Name and Rate % as 1 and click Save button"                |  "should display "Scheme Name is required" in Brokerage Scheme page."     												|
|  IINV_MOS-64  |"input valid Scheme Name, input blank value for Rate % and Min Amount and click Save button"|  "should display Either Rate % or Minimum Brokerage Amount is required."     												|
|  IINV_MOS-65  |           "input Rate % between 0.00001 to 99.99999 and click Save button"                 |  "should save the data and display Saved message."     												|
|  IINV_MOS-66  |           " input Rate % outside range of 0.00001 to 99.99999 and click Save button"       |  "should display "Rate % must be between 0.00001 to 99.99999" in Brokerage Scheme page for rate."     												|
|  IINV_MOS-67  |           " input valid Min Amount and click Save button"                                  |  "should save the data and display Saved message."     												|
|  IINV_MOS-68  |                        " input invalid Min Amount "                                        |  "should display Invalid input values for Min Amount is not entered in the field."     												|


@brokerage_scheme@regression
Scenario: Verify fields displayed in Brokerage Scheme Maintenance page.
Given Verify Testcase with testid "IINV_MOS-69"
Given Logon to MOS application with valid user credentials.
Then Click on the System -> Brokerage Scheme page.
When In Brokerage Scheme Maintenance page, click on plus icon.
Then verify fields in Add Brokerage Scheme page.



@brokerage_scheme@regression
Scenario Outline: Verify fields displayed in Brokerage Scheme Maintenance page.
Given Verify Testcase with testid "<TestcaseID>"
Given Logon to MOS application with valid user credentials.
Then Click on the System -> Brokerage Scheme page.
When In Brokerage Scheme Maintenance page, click on plus icon.
When user "<action>"
And  System "<result>"


Examples:
|   TestcaseID  |                          action                                                                                   |                      result                                                                           |
|  IINV_MOS-70  |" input valid values for Scheme Code, Scheme Name, Rate %, Min Amount and click Save"                              |  "should save the data and display Saved message."                                                    |
|  IINV_MOS-71  |" modify Margin Class Name and click Reload button "                                                               |  "should retrieve back the original data in add brokerage page."                                                            |
|  IINV_MOS-72  |" input valid values for Scheme Code, Scheme Name, Rate %, Min Amount and click on Save"                              |  "should save the data and display Saved message."                                                    |
|  IINV_MOS-73  |" input blank value for Scheme Code, valid Scheme Name and Rate % as 1 and click Save button"                      |  "should display "Scheme Code is required" in Brokerage Scheme page for Code."                        |
|  IINV_MOS-74  |" input valid Scheme Code, valid Scheme Name,  input blank value for Rate % and Min Amount and click Save button"  |  "should display Either Rate % or Minimum Brokerage Amount is required."                              |
|  IINV_MOS-75  |" input Rate % between 0.00001 to 99.99999 ad valid values for other fields and click Save button"                 |  "should save the data and display Saved message."                              |
|  IINV_MOS-76  |" input Rate % outside range of 0.00001 to 99.99999 and valid values for other fields and click Save"              |  "should display "Rate % must be between 0.00001 to 99.99999" in Brokerage Scheme page for rate."                              |
|  IINV_MOS-77  |" input valid Min Amount ad valid values for other fields and click Save"                                          |  "should save the data and display Saved message."                              |
|  IINV_MOS-78  |" input invalid Min Amount ad valid values for other fields"                                                       |  "should display Invalid input values for Min Amount is not entered in the field."                              |
|  IINV_MOS-79  |" input blank value for Min Amount, Rate % and valid values for other fields"                                      |  "should display Either Rate % or Minimum Brokerage Amount is required."                              |
|  IINV_MOS-80  |                   "empty all in add brokerage page."                                                              |  "should retrieve back the original data in addbrokeragePage."                              |

