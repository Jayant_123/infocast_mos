Feature: Testing Infocast MOS for validation for SYSTEM menu.

@validation@regression
Scenario Outline: Validate the min and max values for Password history in Password policy Maintenance page.
Given Verify Testcase with testid "<TestcaseID>"
Given Logon to MOS application with valid user credentials.
Then Click on the System -> Company module.
Then Click on Password policy link.
When Input "<value>" in Password history and click on Save button.
Then System displays "<message>".
Then logout from application.


Examples:

|TestcaseID   |   value  |                     message                      |
| IINV_MOS-1  |     0    |                      Saved.                      |
| IINV_MOS-2  |     13   |     "Password History" must be between 0 to 12.   |
| IINV_MOS-3  |          |           "Password History" is required.        |

@validation@regression
Scenario Outline: Validate the min and max values for Password Expiry day in Password policy Maintenance page.
Given Verify Testcase with testid "<TestcaseID>"
Given Logon to MOS application with valid user credentials.
Then Click on the System -> Company module.
Then Click on Password policy link.
When Input "<value>" in Password Expiry Day and click on Save button.
Then System displays "<message>".
Then logout from application.

Examples:

|TestcaseID   |   value  |                     message                           |
| IINV_MOS-4  |     0    |                      Saved.                           |
| IINV_MOS-5  |    181   |     "Password Expiry Day" must be between 0 to 180.   |
| IINV_MOS-6  |          |           "Password Expiry Day" is required.          |

@validation@regression
Scenario Outline: Validate the min and max values for Max.Failure count in Password policy Maintenance page.
Given Verify Testcase with testid "<TestcaseID>"
Given Logon to MOS application with valid user credentials.
Then Click on the System -> Company module.
Then Click on Password policy link.
When Input "<value>" in Max.Failure count and click on Save button.
Then System displays an error message "<message>".
Then logout from application.

Examples:

|TestcaseID   |   value  |                     message                           |
| IINV_MOS-7  |     0    |       "Max. Failure Count" must not be zero.          |
| IINV_MOS-8  |    13    |     "Max. Failure Count" must be between 1 to 12.     |
| IINV_MOS-9  |          |           "Max. Failure Count" is required.           |


@validation@regression
Scenario Outline: Validate the min values for Min Length in Password policy Maintenance page.
Given Verify Testcase with testid "<TestcaseID>"
Given Logon to MOS application with valid user credentials.
Then Click on the System -> Company module.
Then Click on Password policy link.
When Input "<value>" in Min.Lenght count and click on Save button.
Then System displays "<message>".
Then logout from application.

Examples:

| TestcaseID  |   value  |                     message                           |
| IINV_MOS-10 |     0    |           "Min. Length" must not be zero.             |
| IINV_MOS-11 |    20    |        "Min. Length" must be between 1 to 19.         |
| IINV_MOS-12 |          |           "Min. Length" is required.                  |


@validation@regression
Scenario Outline: Validate the min values for Max Length in Password policy Maintenance page.
Given Verify Testcase with testid "<TestcaseID>"
Given Logon to MOS application with valid user credentials.
Then Click on the System -> Company module.
Then Click on Password policy link.
When Input "<value>" in Max.Lenght count and click on Save button.
Then System displays "<message>".
Then logout from application.

Examples:

| TestcaseID  |   value  |                     message                           |
| IINV_MOS-13 |     0    |           "Max. Length" must not be zero.              |
| IINV_MOS-14 |    20    |       "Max. Length" must be between 1 to 19.           |
| IINV_MOS-15 |          |           "Max. Length" is required.                   |


@validation@regression
Scenario Outline: Validate the min and max values for Max. inactive days in Password policy Maintenance page.
Given Verify Testcase with testid "<TestcaseID>"
Given Logon to MOS application with valid user credentials.
Then Click on the System -> Company module.
Then Click on Password policy link.
When Input "<value>" in  Max. Inactive days and click on Save button.
Then System displays "<message>".
Then logout from application.

Examples:

| TestcaseID  |   value  |                     message                           |
| IINV_MOS-17 |     0    |                      Saved.                           |
| IINV_MOS-18 |    181   |     "Max. Inactive Day" must be between 0 to 180.     |
| IINV_MOS-19 |          |           "Max. Inactive Day" is required.            |


@validation@regression
Scenario Outline: Validate the min and max values for Change Password alert days in Password policy Maintenance page.
Given Verify Testcase with testid "<TestcaseID>"
Given Logon to MOS application with valid user credentials.
Then Click on the System -> Company module.
Then Click on Password policy link.
When Input "<value>" in Change Password alert days and click on Save button.
Then System displays "<message>".
Then logout from application.

Examples:

| TestcaseID  |   value  |                     message                           |
| IINV_MOS-20 |     0    |                      Saved.                           |
| IINV_MOS-21 |    181   |"Change Password Alert Day" must be between 0 to 180.  |
| IINV_MOS-22 |          |          "Change Password Alert Day" is required.     |

@verification@regression
Scenario: Verify the save button functionality in Password policy maintenance page.
Given Verify Testcase with testid "IINV_MOS-23"
Given Logon to MOS application with valid user credentials.
Then Click on the System -> Company module.
Then Click on Password policy link.
When User performs some modifications and click on Save button.
Then System should save the data and display "Saved." message.
Then logout from application.


@verification@regression
Scenario: Verify the Reload button functionality in Password policy maintenance page.
Given Verify Testcase with testid "IINV_MOS-24"
Given Logon to MOS application with valid user credentials.
Then Click on the System -> Company module.
Then Click on Password policy link.
When User performs some modifications and click on Reload button without saving the data.
Then System should able to undo the changes and retrieve back the original values.
Then logout from application.


@verification@regression
Scenario: Verify the Company Link functionality in Password policy Maintenance page.
Given Verify Testcase with testid "IINV_MOS-25"
Given Logon to MOS application with valid user credentials.
Then Click on the System -> Company module.
Then Click on Password policy link.
When User clicks on the Company link in password policy maintenance page.
Then System should navigate back to company Maintenance page.
Then logout from application.

@verification@regression
Scenario: Verify the settings displayed in Password Policy Maintenance page.
Given Verify Testcase with testid "IINV_MOS-26"
Given Logon to MOS application with valid user credentials.
Then Click on the System -> Company module.
Then Click on Password policy link.
Then The password policy maintenance page should be displayed with all the validation fields.
Then logout from application.


