Feature: Testing Infocast MOS for validation for USER ACCOUNT menu.

@user_account@regression
Scenario: Validate the min and max values for Password history in Password policy Maintenance page.
Given Verify Testcase with testid "IINV_MOS-27"
Given Logon to MOS application with valid user credentials.
Then Click on the User -> User Account page.
When In User Account page, enter valid User ID and click on Search button.
Then The validation columns are displayed in Result tab
When In Results tab,User clicks on the first entry.
Then User Account page is displayed with validation fields.
Then logout from application.

@user_account@regression
Scenario Outline: Verify and validate different functionality in User Account page.
Given Verify Testcase with testid "<TestcaseID>"
Given Logon to MOS application with valid user credentials.
Then Click on the User -> User Account page.
When In User Account page, enter valid User ID and click on Search button.
When In Results tab,User clicks on the first entry.
Then In User Account page,user clicks "<button_name>" button
When user "<action>"
And  System "<result>"

Examples:

|   TestcaseID  |       button_name       |                   action                                                   |                      result                                                                        |
|  IINV_MOS-28  |        User Name        |  "modify UserName & clicks Save button"                                    |  "should save the data and display Saved message."                                                 |
|  IINV_MOS-29  |        User Name        |  "modify UserName & clicks Reload button"                                  |  "should retrieve back the original data."                                                         |
|  IINV_MOS-30  |        User Name        |  "input valid value for User Name, and click Save button"                  |  "should save the data and display Saved message."                                                 |
|  IINV_MOS-31  |        User Name        |  "input blank value for User Name and click Save button"                   |  "should display "User Name is required"."                                                         |
|  IINV_MOS-32  |       User Status       |  "clicks on drop down field User Status"                                   |  "should display Active, Suspended, Expired, Closed"                                               |
|  IINV_MOS-33  |        User Mode        |  "clicks on drop down field User Mode"                                     |  "should display Admin Support, None, HK, CN (Full), etc."                                         |
|  IINV_MOS-34  |        User Role        |  "clicks on drop down field User Role"                                     |  "should display PM Group, QA Team, Sales, etc."                                                   |
|  IINV_MOS-35  |      Dealer Group       |  "clicks on drop down field Dealer Group"                                  |  "should display dealer groups."                                                                   |
|  IINV_MOS-36  |       Group link        |  "clicks on User Group link"                                               |  "should display Mandatory fields and tables."                                                     |
|  IINV_MOS-37  |       Group link        |  "clicks on User Group link"                                               |  "should display Mandatory fields and tables."                                                     |
|  IINV_MOS-38  |       Group link        |  "clicks on an entry in User Group belonging column and click on > button" |  "selected entry in User Group - belonging column is moved to User Group - not belonging column"   |
|  IINV_MOS-39  |       Group link        |  "clicks on an entry in User Group belonging column and click on >> button"|  "All entries in User Group - belonging column is moved to User Group - not belonging column"      |
|  IINV_MOS-40  |       Group link        |  "clicks on an entry in User Group belonging column and click on < button" |  "selected entry in User Group - not belonging column is moved to User Group - belonging column"   |
|  IINV_MOS-41  |       Group link        |  "clicks on an entry in User Group belonging column and click on << button"|  "All entries in User Group - not belonging column is moved to User Group - belonging column"      |
|  IINV_MOS-42  |       Group link        |  "clicks on User Account link"					        	               |  "should go back to User Account page."     														|
|  IINV_MOS-43  |     Trading Limit       |  "click on Trading Limit link "						                       |  "should display Mandatory fields and tables in trading Limit page."     														|


@user_account@regression
Scenario: Verify fields displayed in Add User Account page.
Given Verify Testcase with testid "IINV_MOS-44"
Given Logon to MOS application with valid user credentials.
Then Click on the User -> User Account page.
When In User Account page,click on plus icon.
Then  Add User Account page is displayed with new user validation fields.

@user_account@regression
Scenario Outline: Validate fields in Add User Account page.
Given Verify Testcase with testid "<TestcaseID>"
Given Logon to MOS application with valid user credentials.
Then Click on the User -> User Account page.
When In User Account page,click on plus icon.
Then In User Account page,user clicks "<button_name>" button
When user "<action>"
And  System "<result>"

Examples:
|   TestcaseID  |       button_name       |                   action                                                                |                      result                                                                        |
|  IINV_MOS-45  |        User ID          |  "input valid value for User ID, valid value for other fields and click Save button"    |  "should save the data and display Saved message."                                                 |
|  IINV_MOS-46  |        User ID          |  "input blank value for User ID, valid value for other fields and click Save button"    |  "should display "User ID is required" in addUser Page for ID."                                                 |
|  IINV_MOS-47  |        User Name        |  "input valid value for User Name, valid value for other fields and click Save button"  |  "should save the data and display Saved message."                                      |
|  IINV_MOS-48  |        User Name        |  "input blank value for User Name, valid value for other fields and click Save button"  |  "should display "User Name is required" in addUser Page for Name."                                                 |
|  IINV_MOS-49  |       User Status       |  "clicks on drop down field User Status"                                                |  "should display Active, Suspended, Expired, Closed"                                                 |
|  IINV_MOS-51  |        User Mode        |  "clicks on drop down field User Mode"                                                  |  "should display Admin Support, None, HK, CN (Full), etc."                                         |
|  IINV_MOS-52  |        User Role        |  "clicks on drop down field User Role"                                                  |  "should display PM Group, QA Team, Sales, etc."                                                   |
|  IINV_MOS-53  |      Dealer Group       |  "clicks on drop down field Dealer Group"                                               |  "should display dealer groups."                                                                   |
|  IINV_MOS-54  |       User Name         |  "modify UserName & clicks Reload button"                                               |  "should retrieve back the original data."                                                                   |
|  IINV_MOS-55  |     User Password       |  "input valid value for User Password, same value for Confirm Password"                 |  "should save the data and display Saved message."                                                                   |
|  IINV_MOS-56  |    Confirm Password     |  "input blank value for Confirm Password"                                               |  "should display "Confirm Password is required" in addUser Page for Confirm Password."                                                                   |
|  IINV_MOS-57  |     User Password       |  "input different value for User Password and confirm password"                         |  "should display "User Password" does not match "Confirm Password"."                                                                   |

