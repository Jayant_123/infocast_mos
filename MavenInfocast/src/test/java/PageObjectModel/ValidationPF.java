package PageObjectModel;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import stepDefinitions.Hooks;
import utility.BusinessFunctions;


public class ValidationPF {



    public static WebElement element = null;

    // username
    public static WebElement username() {
        element = BusinessFunctions.getWebDriver().findElement(By.id("userId"));
        return element;
    }

    // password
    public static WebElement password() {
        element = BusinessFunctions.getWebDriver().findElement(By.id("password"));
        return element;
    }

    // submit
    public static WebElement submit() {
        element = BusinessFunctions.getWebDriver().findElement(By.id("submit"));
        return element;
    }

    // homepage
    public static WebElement homepage() {
        element = BusinessFunctions.getWebDriver().findElement(By.tagName("Home"));
        return element;
    }

    // system
    public static WebElement system() {
       //BusinessFunctions.getWebDriver().findElement(By.xpath("//span[text()='System']"));
        element = BusinessFunctions.getWaitDriver().until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='menuArea']/ul[1]/li/span[1][text()='System']")));
        return element;
    }

    //reload
    public static WebElement reload() {
        //BusinessFunctions.getWebDriver().findElement(By.xpath("//span[text()='System']"));
        element = BusinessFunctions.getWaitDriver().until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='reload']")));
        return element;
    }

    // company
    public static WebElement company() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//span[text()='Company']"));
        return element;
    }

    // companyMaintainancePage
    public static WebElement companyMaintainancePage() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//div[text()='Company Maintenance']"));
        return element;
    }

    // maxInactiveDay
    public static WebElement maxInactiveDay() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='maxInactiveDay']"));
        return element;
    }

    // changePasswordAlertDay
    public static WebElement changePasswordAlertDay() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='changePasswordAlertDay']"));
        return element;
    }

       // logout
    public static WebElement logout() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//a[text()='Logout']"));
        return element;
    }

    // maxFailureCount
    public static WebElement maxFailureCount() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='maxFailureCount']"));
        return element;
    }

    // backtoCompanyPage
    public static WebElement backtoCompanyPage() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//a[text()=' << Company']"));
        return element;
    }

    // companyMaintenancePage
    public static WebElement companyMaintenancePage() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//div[text()='Company Maintenance']"));
        return element;
    }

    // passwordPolicy
    public static WebElement passwordPolicy() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//a[text()='Password Policy >>']"));
        return element;
    }


    // passwordHistoryTag
    public static WebElement passwordHistoryTag() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='passwordHistory']"));
        return element;
    }

    // passwordHistory
    public static WebElement passwordHistory() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='passwordHistory']"));
        return element;
    }
    public static WebElement messageSaved() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//*[@id=\"ui-id-1\"]"));
        return element;
    }

    // messageNotSaved
    public static WebElement messageNotSaved() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//*[@id=\"ui-id-1\"]"));
        return element;
    }

    // messageEmptyPassHistory
    public static WebElement messageEmptyPassHistory() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//*[@id=\"ui-id-1\"]"));
        return element;
    }


    // passwordSave
    public static WebElement passwordSave() {
        element = BusinessFunctions.getWebDriver().findElement(By.name("save"));
        return element;
    }

    // okButton
    public static WebElement okButton() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//span[text()='OK']"));
        return element;
    }

    // passExpiryButton
    public static WebElement passExpiryButton() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='passwordExpiryDay']"));
        return element;
    }

    // passHistoryMessage
    public static WebElement passHistoryMessage() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//*[@id=\"ui-id-1\"]"));
        return element;
    }

    // passHistorynovalue
    public static WebElement passHistorynovalue() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//*[@id=\"ui-id-1\"]"));
        return element;
    }


    // maxFailCount
    public static WebElement maxFailCount() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='maxFailureCount']"));
        return element;
    }

    // minLength
    public static WebElement minLength() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='minLength']"));
        return element;
    }

    // maxLength
    public static WebElement maxLength() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='maxLength']"));
        return element;
    }

    // message
    public static WebElement message() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//div[contains(@id,'ui-id-')]"));
        return element;
    }
    // maxFailCountTag
    public static WebElement maxFailCountTag() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("////div[text()='Max. Failure Count']"));
        return element;
    }


    // passExpiryButtonTag
    public static WebElement passExpiryButtonTag() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//div[text()='Password Expiry Day']"));
        return element;
    }

}
