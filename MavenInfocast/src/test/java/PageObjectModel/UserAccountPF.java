package PageObjectModel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utility.BusinessFunctions;

public class UserAccountPF {

    public static WebElement element = null;

    // userMenu
    public static WebElement userMenu() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//span[text()='User'][1]"));
        return element;
    }

    // userAccountMenu
    public static WebElement userAccountMenu() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//span[text()='User Account'][1]"));
        return element;
    }

    // searchMenu
    public static WebElement searchMenu() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//a[text()='Search']"));
        return element;
    }


    // searchUserId
    public static WebElement searchUserId() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='searchUserId']"));
        return element;
    }


    // searchUserName
    public static WebElement searchUserName() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='searchUserName']"));
        return element;
    }

    // search
    public static WebElement search() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='search']"));
        return element;
    }

    // clear
    public static WebElement clear() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='clear']"));
        return element;
    }

    // userDetailsRow
    public static WebElement userDetailsRow() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//td[text()='ACHAN']"));
        return element;
    }



    // userAccountPage
    public static WebElement userAccountPage() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//div[text()='User Account']"));
        return element;
    }



    // userIDCol
    public static WebElement userIDCol() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//text[text()='User ID']"));
        return element;
    }

    // userID
    public static WebElement userID() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='userId']"));
        return element;
    }

    // userName
    public static WebElement userName() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='userName']"));
        return element;
    }

    // userPassword
    public static WebElement userPassword() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='userPassword'][2]"));
        return element;
    }
    // confirmPassword
    public static WebElement confirmPassword() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='confirmPassword'][2]"));
        return element;
    }


    // userStatus
    public static WebElement userStatus() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//select[@name='userStatus']"));
        return element;
    }

    // userMode
    public static WebElement userMode() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//select[@name='SZquoteGroup'][1]"));
        return element;
    }

    // userModeCheckbox
    public static WebElement userModeCheckbox() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='SZquoteInd'][1]"));
        return element;
    }

    // userModeTrader
    public static WebElement SHquoteGroup() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//select[@name='SHquoteGroup']"));
        return element;
    }



    // userRole
    public static WebElement userRole() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//select[@name='roleId']"));
        return element;
    }

    // dealerGroup
    public static WebElement dealerGroup() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//select[@name='dealerGroup']"));
        return element;
    }


    // userGroup
    public static WebElement userGroup() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//a[text()='User Group >>']"));
        return element;
    }


    // userGroupPage
    public static WebElement userGroupTable() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//table//th[text()='User Group (Belonging)']"));
        return element;
    }

    // saveinGroup
    public static WebElement saveinGroup() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='save'][1]"));
        return element;
    }

    // reloadinGroup
    public static WebElement reloadinGroup() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='reload'][1]"));
        return element;
    }

    // removeAllGroupsFromUser
    public static WebElement removeAllGroupsFromUser() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//button[@name='removeAllGroup']"));
        return element;
    }

    // addAllGroupsToUser
    public static WebElement addAllGroupsToUser() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//button[@name='addAllGroupToUser']"));
        return element;
    }

    // removeGroupFromUser
    public static WebElement removeGroupFromUser() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//button[@name='removeGroupFromUser']"));
        return element;
    }

    // addGrouptoUser
    public static WebElement addGrouptoUser() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//button[@name='addGroupToUser']"));
        return element;
    }


    // belongingUser
    public static WebElement belongingGroup() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//table//td[@class='col col1 left']"));
        return element;
    }


    // notbelongingUser
    public static WebElement notbelongingGroup() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//table[@name='groupNotBelongingList']//td[@class='col col1 left'][1]"));
        return element;
    }



    // notbelongingUser
    public static WebElement backtoUserAccountPage() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//a[text()='<< User Account']"));
        return element;
    }

    // tradingLimit
    public static WebElement tradingLimit() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//a[text()='Trading Limit >>']"));
        return element;
    }

    // tradingLimitPage
    public static WebElement tradingLimitPage() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//div[text()='User Account Trading Limit']"));
        return element;
    }

    // tradingLimitPage
    public static WebElement addUser() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//img[@name='add']"));
        return element;
    }

    // tradingLimitPage
    public static WebElement addUserPage() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//div[text()='Add User']"));
        return element;
    }



}
