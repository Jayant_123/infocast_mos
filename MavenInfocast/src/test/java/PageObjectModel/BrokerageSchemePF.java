package PageObjectModel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utility.BusinessFunctions;

public class BrokerageSchemePF {


    public static WebElement element = null;

    // brokerageScheme
    public static WebElement brokerageScheme() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//span[text()='Brokerage Scheme'][1]"));
        return element;
    }


    // searchBkgSchCode
    public static WebElement bkgSchCode() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//div[text()='Scheme Code']"));
        return element;
    }

    // searchBkgSchName
    public static WebElement bkgSchName() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//div[text()='Scheme Name']"));
        return element;
    }

    // searchTab
    public static WebElement searchTab() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//a[text()='Search']"));
        return element;
    }

    // resultTab
    public static WebElement resultTab() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//a[text()='Result']"));
        return element;
    }

    // codeInResultTab
    public static WebElement codeInResultTab() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//th[@class='col col1']"));
        return element;
    }

    // searchBkgSchCode
    public static WebElement searchBkgSchCode() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='searchBkgSchCode']"));
        return element;
    }

    // searchBkgSchName
    public static WebElement searchBkgSchName() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='searchBkgSchName']"));
        return element;
    }

    // clear
    public static WebElement clear() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='clear']"));
        return element;
    }

    // userbkgSchCode
    public static WebElement userbkgSchCode() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='bkgSchCode']"));
        return element;
    }

    // userBkgSchName
    public static WebElement userBkgSchName() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='bkgSchName']"));
        return element;
    }

    // userBkgSchRate
    public static WebElement userBkgSchRate() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='flatRatePct']"));
        return element;
    }

    // userBkgSchAmount
    public static WebElement userBkgSchAmount() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='minBkgAmt']"));
        return element;
    }

    // search
    public static WebElement search() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='search']"));
        return element;
    }

    // firstEntry
    public static WebElement firstEntry() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//td[@class='col col2 center'][1]"));
        return element;
    }

    // reload
    public static WebElement reload() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='reload']"));
        return element;
    }

    // delete
    public static WebElement delete() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='delete']"));
        return element;
    }

    // save
    public static WebElement save() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//input[@name='save']"));
        return element;
    }

    // addBrokerageSchemePage
    public static WebElement addBrokerageSchemePage() {
        element = BusinessFunctions.getWebDriver().findElement(By.xpath("//div[text()='Add Brokerage Scheme']"));
        return element;
    }


}
