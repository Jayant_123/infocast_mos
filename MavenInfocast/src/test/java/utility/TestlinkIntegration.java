package utility;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;
import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import br.eti.kinoshita.testlinkjavaapi.model.TestCase;
import br.eti.kinoshita.testlinkjavaapi.model.TestCaseStep;
import net.rcarz.jiraclient.BasicCredentials;
import net.rcarz.jiraclient.Field;
import net.rcarz.jiraclient.Issue;
import net.rcarz.jiraclient.JiraClient;
import net.rcarz.jiraclient.JiraException;
import testlink.api.java.client.TestLinkAPIClient;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIHelper;
import testlink.api.java.client.TestLinkAPIResults;



public class TestlinkIntegration {
	
	public static String testlinkURL = System.getProperty("testlink_url");
	public static String testlinkPROJECT = System.getProperty("testlink_project_name");
	public static String testlinkPLAN = System.getProperty("testlink_plan_name");
	public static String jiraUSERNAME = System.getProperty("jira_username");
	public static String jiraPASSWORD = System.getProperty("jira_password");
	public static String jiraURL = System.getProperty("jira_url");
	
	public static final String TESTLINK_KEY = "daa88d6c650efc6013f88fbcbd64fc77";
	public static final String TESTLINK_URL = testlinkURL ;
	public static final String TEST_PROJECT_NAME = testlinkPROJECT ;
	public static final String TEST_PLAN_NAME = testlinkPLAN ;

	public static String bugID = null;
	
	public static TestLinkAPIClient testlink = new TestLinkAPIClient(TESTLINK_KEY,TESTLINK_URL);
	
	public static void updateResults(String arg1, String exception, String results) throws TestLinkAPIException,java.io.IOException
	{
		
	try {
		
		URL url1 =new URL("http://localhost/testlink/lib/api/xmlrpc/v1/xmlrpc.php"); 
		TestLinkAPIClient testlink = new TestLinkAPIClient(TESTLINK_KEY,TESTLINK_URL);
		TestLinkAPIHelper help = new TestLinkAPIHelper();
		TestLinkAPI api = new TestLinkAPI(url1, TESTLINK_KEY);


		
		final String BUILD_NO =String.valueOf(testlink.getLatestBuildForTestPlan(TEST_PROJECT_NAME, TEST_PLAN_NAME));
		String[] w=BUILD_NO.split(",");
		String Build = w[3].replace("name=", "").trim();

		System.out.println("Build Name- "+Build);

		/*final String testcase_platform = String.valueOf(testlink.getCasesForTestPlan(218));
		
		String[] pf = testcase_platform.split(arg1+",");
		
		String[] pl = pf[1].split("platform_name=");
		
		String[] platform = pl[1].split(",");
		String platf = platform[0]; */
		
		Map<String, String> testLinkEnvVar = new HashMap<String, String>();
		
		testLinkEnvVar.replace("", "");
		
		String platformName = System.getProperty("platform");
		
		BasicCredentials creds = new BasicCredentials(jiraUSERNAME, jiraPASSWORD);
		JiraClient jira = new JiraClient(jiraURL , creds);

		TestCase testcase = api.getTestCaseByExternalId(arg1, 1);

		StringBuilder testCaseInfo = new StringBuilder();  
		testCaseInfo.append(testcase.getSteps());
		String description = testCaseInfo.toString();
		String summary = testcase.getSummary().replace("\n"," ");

		System.out.println("Testlink Project ID - "+TestLinkAPIHelper.getProjectID(testlink,"InfocastMOS"));
		System.out.println("Test Project details"+api.getTestProjectByName("InfocastMOS"));

		//In the testlink updatation we have to change the testprojectID and planID for different projects.
		
		if(results==TestLinkAPIResults.TEST_PASSED)

			api.reportTCResult(help.getTestCaseID(testlink, 223,arg1), null, 224, ExecutionStatus.PASSED, null, Build, null, true, null, null, platformName,testLinkEnvVar , true);
		else
			//LOG BUG IN JIRA
		{
			 Issue.SearchResult sr = jira.searchIssues("assignee=venkatakumar.bonthala");
	          
	            int j=0;
	            for (Issue i : sr.issues)
	            {
	           
	            	if(i.getSummary().contains(testcase.getSummary()) && i.getStatus().getName().contentEquals("Done"))
	            	{	
	            	
	            		i.addComment("issue exist");
	            		System.out.println("-----------------issue exist----------------");
	            		j = 2;
	            		i.transition().execute("To Do");
	            		break;
	            	}
	            	
	            }
	          
	            System.out.println(j);
	            if(j==0 )
	            {
	            Issue newIssue = jira.createIssue("INFOC","Bug").field(Field.SUMMARY,summary)
				        .field(Field.DESCRIPTION,description).execute();
	            bugID = newIssue.getKey();
	            
	            //REPORT IN TESTLINK
	            
	            //api.reportTCResult(help.getTestCaseID(testlink, 1,arg1), null, 218, ExecutionStatus.FAILED, null, Build, null, true, bugID, null, platformName,testLinkEnvVar , true);
	           
	            }
	            api.reportTCResult(help.getTestCaseID(testlink, 223,arg1), null, 224, ExecutionStatus.FAILED, null, Build, bugID, true, bugID, null, platformName,testLinkEnvVar , true);

		}
			
	
			
	} 
	catch (Exception e) {
          	e.printStackTrace();
      }
	}
	

}
