package stepDefinitions;

import PageObjectModel.ValidationPF;
import cucumber.api.PendingException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import utility.BusinessFunctions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Login {

    static String web_app = System.getProperty("web_application");
    static String App_Username = System.getProperty("app_username");
    static String App_Password = System.getProperty("app_password");
    public static String browser = System.getProperty("browser_name");


    @Given("^Verify Testcase with testid \"([^\"]*)\"$")
    public void verify_Testcase_with_testid(String arg1) {
        Hooks.testid = arg1;
    }

    @Given("^Logon to MOS application with valid user credentials\\.$")
    public void logon_to_MOS_application_with_valid_user_credentials() {
        BusinessFunctions.OpenURL(web_app);
        BusinessFunctions.Sendkeys(ValidationPF.username(),App_Username);
        BusinessFunctions.Sendkeys(ValidationPF.password(),App_Password);
        BusinessFunctions.Click(ValidationPF.submit());

    }

    @Then("^Click on the System -> Company module\\.$")
    public void click_on_the_System_Company_module() throws InterruptedException {

        BusinessFunctions.parentFrame();

        Thread.sleep(4000);

        BusinessFunctions.Click(ValidationPF.system());
        Thread.sleep(2000);
        BusinessFunctions.Click(ValidationPF.company());
        Thread.sleep(3000);
        BusinessFunctions.selectFrame("mainIframe");
    }

    @Then("^Click on Password policy link\\.$")
    public void click_on_Password_policy_link() throws InterruptedException {
        Thread.sleep(3000);
        BusinessFunctions.Click(ValidationPF.passwordPolicy());
        Thread.sleep(3000);

    }

    @When("^Input \"([^\"]*)\" in Password history and click on Save button\\.$")
    public void input_in_Password_history_and_click_on_Save_button(String arg1) throws InterruptedException {

        BusinessFunctions.clearField(ValidationPF.passwordHistory());
        BusinessFunctions.Sendkeys(ValidationPF.passwordHistory(),arg1);
        BusinessFunctions.Click(ValidationPF.passwordSave());
        Thread.sleep(2000);

    }

    @Then("^System displays \"([^\"]*)\"\\.$")
    public void system_displays(String arg1) throws InterruptedException {

        Thread.sleep(3000);

            BusinessFunctions.Assert_String_Case(arg1, BusinessFunctions.getText(ValidationPF.message()));

        Thread.sleep(2000);
        BusinessFunctions.Click(ValidationPF.okButton());
            Thread.sleep(2000);
    }

    @Then("^logout from application\\.$")
    public void logout_from_application() throws InterruptedException {

    }

    @Then("^System displays \"([^\"]*)\"Password History\"([^\"]*)\"\\.$")
    public void system_displays_Password_History(String arg1, String arg2) throws InterruptedException {
        Thread.sleep(2000);
        if(BusinessFunctions.getText(ValidationPF.messageNotSaved()).contains("\"Password History\" must be between 0 to 12."))
            BusinessFunctions.Assert_String_Case("\"Password History\" must be between 0 to 12.", BusinessFunctions.getText(ValidationPF.message()));
        else
            BusinessFunctions.Assert_String_Case("\"Password History\" is required.", BusinessFunctions.getText(ValidationPF.message()));

        Thread.sleep(2000);
        BusinessFunctions.Click(ValidationPF.okButton());
        Thread.sleep(2000);
    }

    @When("^Input \"([^\"]*)\" in Password Expiry Day and click on Save button\\.$")
    public void input_in_Password_Expiry_Day_and_click_on_Save_button(String arg1) throws InterruptedException {

        BusinessFunctions.clearField(ValidationPF.passExpiryButton());
        BusinessFunctions.Sendkeys(ValidationPF.passExpiryButton(),arg1);
        BusinessFunctions.Click(ValidationPF.passwordSave());
        Thread.sleep(2000);


    }

    @Then("^System displays \"([^\"]*)\"Password Expiry Day\"([^\"]*)\"\\.$")
    public void system_displays_Password_Expiry_Day(String arg1, String arg2) throws InterruptedException {
        System.out.println("******************"+BusinessFunctions.getText(ValidationPF.message()));
        if(BusinessFunctions.getText(ValidationPF.message()).contains("\"Password Expiry Day\" must be between 0 to 180."))
            BusinessFunctions.Assert_String_Case("\"Password Expiry Day\" must be between 0 to 180.", BusinessFunctions.getText(ValidationPF.passHistoryMessage()));
        else
            BusinessFunctions.Assert_String_Case("\"Password Expiry Day\" is required.", BusinessFunctions.getText(ValidationPF.message()));

        Thread.sleep(2000);
        BusinessFunctions.Click(ValidationPF.okButton());
        Thread.sleep(2000);

    }

    @When("^Input \"([^\"]*)\" in Max\\.Failure count and click on Save button\\.$")
    public void input_in_Max_Failure_count_and_click_on_Save_button(String arg1) throws InterruptedException {
        BusinessFunctions.clearField(ValidationPF.maxFailureCount());
        BusinessFunctions.Sendkeys(ValidationPF.maxFailureCount(),arg1);
        BusinessFunctions.Click(ValidationPF.passwordSave());
        Thread.sleep(2000);
    }


    @Then("^System displays \"([^\"]*)\"Max\\.Failure\"([^\"]*)\"\\.$")
    public void system_displays_Max_Failure(String arg1, String arg2) throws InterruptedException {

        BusinessFunctions.Assert_String_Case("\"Max.Failure Count\" is required.", BusinessFunctions.getText(ValidationPF.message()));
        Thread.sleep(2000);
        BusinessFunctions.Click(ValidationPF.okButton());
        Thread.sleep(2000);

    }


    @Then("^System displays an error message \"([^\"]*)\"Max\\. Failure Count\"([^\"]*)\"\\.$")
    public void system_displays_an_error_message_Max_Failure_Count(String arg1,String arg2) throws InterruptedException {

        Thread.sleep(2000);
        if(BusinessFunctions.getText(ValidationPF.message()).contains("\"Max. Failure Count\" must not be zero."))
            BusinessFunctions.Assert_String_Case("\"Max. Failure Count\" must not be zero.", BusinessFunctions.getText(ValidationPF.message()));
        else if(BusinessFunctions.getText(ValidationPF.message()).contains("\"Max. Failure Count\" must be between 1 to 12."))
            BusinessFunctions.Assert_String_Case("\"Max. Failure Count\" must be between 1 to 12.", BusinessFunctions.getText(ValidationPF.message()));
        else
            BusinessFunctions.Assert_String_Case("\"Max. Failure Count\" is required.", BusinessFunctions.getText(ValidationPF.message()));


        Thread.sleep(2000);
        BusinessFunctions.Click(ValidationPF.okButton());
        Thread.sleep(2000);

    }

    @When("^Input \"([^\"]*)\" in Min\\.Lenght count and click on Save button\\.$")
    public void input_in_Min_Lenght_count_and_click_on_Save_button(String arg1) throws InterruptedException {
        BusinessFunctions.clearField(ValidationPF.minLength());
        BusinessFunctions.Sendkeys(ValidationPF.minLength(),arg1);
        BusinessFunctions.Click(ValidationPF.passwordSave());
        Thread.sleep(2000);

    }

    @Then("^System displays \"([^\"]*)\"Min\\. Length\"([^\"]*)\"\\.$")
    public void system_displays_Min_Length(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(2000);
        if(BusinessFunctions.getText(ValidationPF.message()).contains("\"Min. Length\" must not be zero."))
            BusinessFunctions.Assert_String_Case("\"Min. Length\" must not be zero.", BusinessFunctions.getText(ValidationPF.message()));
        else if(BusinessFunctions.getText(ValidationPF.message()).contains("\"Min. Length\" must be between 1 to 19."))
            BusinessFunctions.Assert_String_Case("\"Min. Length\" must be between 1 to 19.\n" +
                    "\"Min. Length\" must not be larger than \"Max. Length\".", BusinessFunctions.getText(ValidationPF.message()));
        else
            BusinessFunctions.Assert_String_Case("\"Min. Length\" is required.", BusinessFunctions.getText(ValidationPF.message()));


        Thread.sleep(2000);
        BusinessFunctions.Click(ValidationPF.okButton());
        Thread.sleep(2000);

    }

    @When("^Input \"([^\"]*)\" in Max\\.Lenght count and click on Save button\\.$")
    public void input_in_Max_Lenght_count_and_click_on_Save_button(String arg1) throws InterruptedException {
        BusinessFunctions.clearField(ValidationPF.maxLength());
        BusinessFunctions.Sendkeys(ValidationPF.maxLength(),arg1);
        BusinessFunctions.Click(ValidationPF.passwordSave());
        Thread.sleep(2000);
    }

    @Then("^System displays \"([^\"]*)\"Max\\. Length\"([^\"]*)\"\\.$")
    public void system_displays_Max_Length(String arg1, String arg2) throws InterruptedException {
        Thread.sleep(2000);
        if(BusinessFunctions.getText(ValidationPF.message()).contains("\"Max. Length\" must not be zero."))
            BusinessFunctions.Assert_String_Case("\"Max. Length\" must not be zero.\n" +
                    "\"Min. Length\" must not be larger than \"Max. Length\".", BusinessFunctions.getText(ValidationPF.message()));
        else if(BusinessFunctions.getText(ValidationPF.message()).contains("\"Max. Length\" must be between 1 to 19."))
            BusinessFunctions.Assert_String_Case("\"Max. Length\" must be between 1 to 19.", BusinessFunctions.getText(ValidationPF.message()));
        else
            BusinessFunctions.Assert_String_Case("\"Max. Length\" is required.", BusinessFunctions.getText(ValidationPF.message()));


        Thread.sleep(2000);
        BusinessFunctions.Click(ValidationPF.okButton());
        Thread.sleep(2000);


    }

    @When("^Input \"([^\"]*)\" in  Max\\. Inactive days and click on Save button\\.$")
    public void input_in_Max_Inactive_days_and_click_on_Save_button(String arg1) throws InterruptedException {
        BusinessFunctions.clearField(ValidationPF.maxInactiveDay());
        BusinessFunctions.Sendkeys(ValidationPF.maxInactiveDay(),arg1);
        BusinessFunctions.Click(ValidationPF.passwordSave());
        Thread.sleep(2000);

    }

    @Then("^System displays \"([^\"]*)\"Max\\. Inactive Day\"([^\"]*)\"\\.$")
    public void system_displays_Max_Inactive_Day(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(2000);
        if(BusinessFunctions.getText(ValidationPF.message()).contains("Saved."))
            BusinessFunctions.Assert_String_Case("Saved.", BusinessFunctions.getText(ValidationPF.message()));
        else if(BusinessFunctions.getText(ValidationPF.message()).contains("\"Max. Inactive Day\" must be between 0 to 180."))
            BusinessFunctions.Assert_String_Case("\"Max. Inactive Day\" must be between 0 to 180.", BusinessFunctions.getText(ValidationPF.message()));
        else
            BusinessFunctions.Assert_String_Case("\"Max. Inactive Day\" is required.", BusinessFunctions.getText(ValidationPF.message()));


        Thread.sleep(2000);
        BusinessFunctions.Click(ValidationPF.okButton());
        Thread.sleep(2000);

    }



    @When("^Input \"([^\"]*)\" in Change Password alert days and click on Save button\\.$")
    public void input_in_Change_Password_alert_days_and_click_on_Save_button(String arg1) throws InterruptedException {
        Thread.sleep(2000);

        BusinessFunctions.clearField(ValidationPF.changePasswordAlertDay());
        BusinessFunctions.Sendkeys(ValidationPF.changePasswordAlertDay(),arg1);
        BusinessFunctions.Click(ValidationPF.passwordSave());
        Thread.sleep(2000);

    }

    @Then("^System displays \"([^\"]*)\"Change Password Alert Day\"([^\"]*)\"\\.$")
    public void system_displays_Change_Password_Alert_Day(String arg1, String arg2) throws InterruptedException {
        Thread.sleep(2000);
        if(BusinessFunctions.getText(ValidationPF.message()).contains("Saved."))
            BusinessFunctions.Assert_String_Case("Saved.", BusinessFunctions.getText(ValidationPF.message()));
        else if(BusinessFunctions.getText(ValidationPF.message()).contains("\"Change Password Alert Day\" must be between 0 to 180."))
            BusinessFunctions.Assert_String_Case("\"Change Password Alert Day\" must be between 0 to 180.\n" +
                    "\"Password Expiry Day\" must not be smaller than \"Change Password Alert Day\".", BusinessFunctions.getText(ValidationPF.message()));
        else
            BusinessFunctions.Assert_String_Case("\"Change Password Alert Day\" is required.", BusinessFunctions.getText(ValidationPF.message()));


        Thread.sleep(2000);
        BusinessFunctions.Click(ValidationPF.okButton());
        Thread.sleep(2000);
    }




}
