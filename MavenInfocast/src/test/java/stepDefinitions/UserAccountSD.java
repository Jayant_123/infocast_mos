package stepDefinitions;

import PageObjectModel.UserAccountPF;
import PageObjectModel.ValidationPF;
import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.lexer.Th;
import utility.BusinessFunctions;

public class UserAccountSD {


    public static String original;

    @Then("^Click on the User -> User Account page\\.$")
    public void click_on_the_User_User_Account_page() throws InterruptedException {
        Thread.sleep(2000);
        BusinessFunctions.Click(UserAccountPF.userMenu());
        BusinessFunctions.Click(UserAccountPF.userAccountMenu());
        Thread.sleep(8000);
    }

    @When("^In User Account page, enter valid User ID and click on Search button\\.$")
    public void in_User_Account_page_enter_valid_User_ID_and_click_on_Search_button() throws InterruptedException {

        BusinessFunctions.selectFrame("mainIframe");
        Thread.sleep(2000);
        BusinessFunctions.Click(UserAccountPF.searchMenu());
        Thread.sleep(2000);
        BusinessFunctions.Click(UserAccountPF.searchUserId());
        Thread.sleep(4000);

        BusinessFunctions.Sendkeys(UserAccountPF.searchUserId(),"ACHAN");
        BusinessFunctions.Click(UserAccountPF.search());
    }

    @Then("^The validation columns are displayed in Result tab$")
    public void the_validation_columns_are_displayed_in_Result_tab() {

        BusinessFunctions.Assert_String_Case("User ID",UserAccountPF.userIDCol().getText());


    }

    @When("^In Results tab,User clicks on the first entry\\.$")
    public void in_Results_tab_User_clicks_on_the_first_entry() throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.Click(UserAccountPF.userDetailsRow());
        Thread.sleep(2000);
    }

    @Then("^User Account page is displayed with validation fields\\.$")
    public void user_Account_page_is_displayed_with_validation_fields() {

        BusinessFunctions.Assert_String_Case("User Account",UserAccountPF.userAccountPage().getText());

    }

    @Then("^In User Account page,user clicks \"([^\"]*)\" button$")
    public void in_User_Account_page_user_clicks_button(String arg1) throws InterruptedException {

        Thread.sleep(6000);
 /*       Thread.sleep(6000);
        BusinessFunctions.Click(UserAccountPF.userName());*/
    }

    @When("^user \"([^\"]*)\"modify UserName & clicks Save button\"([^\"]*)\"$")
    public void user_modify_UserName_clicks_Save_button(String arg1, String arg2){
        BusinessFunctions.clearField(UserAccountPF.userName());
        BusinessFunctions.Sendkeys(UserAccountPF.userName(),"Alvin Chan");
        BusinessFunctions.Click(UserAccountPF.saveinGroup());

    }

    @When("^System \"([^\"]*)\"should save the data and display Saved message\\.\"([^\"]*)\"$")
    public void system_should_save_the_data_and_display_Saved_message(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(4000);
        BusinessFunctions.Assert_String_Case("Saved.",BusinessFunctions.getText(ValidationPF.message()));

    }


    @When("^user \"([^\"]*)\"modify UserName & clicks Reload button\"([^\"]*)\"$")
    public void user_modify_UserName_clicks_Reload_button(String arg1, String arg2) throws InterruptedException {

        original = BusinessFunctions.getText(UserAccountPF.userName());
        Thread.sleep(2000);
        BusinessFunctions.clearField(UserAccountPF.userName());
        BusinessFunctions.Sendkeys(UserAccountPF.userName(),"Alvin");

        BusinessFunctions.Click(UserAccountPF.reloadinGroup());
        Thread.sleep(6000);


    }

    @When("^System \"([^\"]*)\"should retrieve back the original data\\.\"([^\"]*)\"$")
    public void system_should_retrieve_back_the_original_data(String arg1, String arg2) {

         BusinessFunctions.Assert_String_Case(original,BusinessFunctions.getText(UserAccountPF.userName()));

    }
/*

    @When("^user \"([^\"]*)\"clicks OK on the popup prompt window to confirm deletion\"([^\"]*)\"$")
    public void user_clicks_OK_on_the_popup_prompt_window_to_confirm_deletion(String arg1, String arg2) {




    }

    @When("^System \"([^\"]*)\"should delete the User Account specific to the entered User ID\"([^\"]*)\"$")
    public void system_should_delete_the_User_Account_specific_to_the_entered_User_ID(String arg1, String arg2) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
*/

    @When("^user \"([^\"]*)\"input valid value for User Name, and click Save button\"([^\"]*)\"$")
    public void user_input_valid_value_for_User_Name_and_click_Save_button(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(3000);
        BusinessFunctions.clearField(UserAccountPF.userName());
        BusinessFunctions.Sendkeys(UserAccountPF.userName(),"Alvin Chan"); Thread.sleep(3000);
        Thread.sleep(3000);
        BusinessFunctions.Click(UserAccountPF.saveinGroup());

    }

    @When("^user \"([^\"]*)\"input blank value for User Name and click Save button\"([^\"]*)\"$")
    public void user_input_blank_value_for_User_Name_and_click_Save_button(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.clearField(UserAccountPF.userName());
        BusinessFunctions.Sendkeys(UserAccountPF.userName(),"");
        Thread.sleep(3000);
        BusinessFunctions.Click(UserAccountPF.saveinGroup());
    }

    @When("^System \"([^\"]*)\"should display \"([^\"]*)\"\\.\"([^\"]*)\"$")
    public void system_should_display(String arg1, String arg2, String arg3) throws InterruptedException {

        Thread.sleep(4000);
        BusinessFunctions.Assert_String_Case("\"User Name\" is required.",BusinessFunctions.getText(ValidationPF.message()));


    }

    @When("^user \"([^\"]*)\"clicks on drop down field User Status\"([^\"]*)\"$")
    public void user_clicks_on_drop_down_field_User_Status(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(4000);
        BusinessFunctions.Click(UserAccountPF.userStatus());


    }

    @When("^System \"([^\"]*)\"should display Active, Suspended, Expired, Closed\"([^\"]*)\"$")
    public void system_should_display_Active_Suspended_Expired_Closed(String arg1, String arg2) throws InterruptedException {
        Thread.sleep(2000);
        BusinessFunctions.Assert_String_Case("Active\n" +
                "Suspended\n" +
                "Expired\n" +
                "Closed",BusinessFunctions.getText(UserAccountPF.userStatus()));

    }

    @When("^user \"([^\"]*)\"clicks on drop down field User Mode\"([^\"]*)\"$")
    public void user_clicks_on_drop_down_field_User_Mode(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(6000);
        BusinessFunctions.Click(UserAccountPF.userModeCheckbox());
        Thread.sleep(2000);
        BusinessFunctions.Click(UserAccountPF.userMode());

    }

    @When("^System \"([^\"]*)\"should display Admin Support, None, HK, CN \\(Full\\), etc\\.\"([^\"]*)\"$")
    public void system_should_display_Admin_Support_None_HK_CN_Full_etc(String arg1, String arg2) throws InterruptedException {


        Thread.sleep(3000);
        BusinessFunctions.Assert_String_Case("Eligible\n" +
                "Full",BusinessFunctions.getText(UserAccountPF.userMode()));

    }

    @When("^user \"([^\"]*)\"clicks on drop down field User Role\"([^\"]*)\"$")
    public void user_clicks_on_drop_down_field_User_Role(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.Click(UserAccountPF.userRole());

    }

    @When("^System \"([^\"]*)\"should display PM Group, QA Team, Sales, etc\\.\"([^\"]*)\"$")
    public void system_should_display_PM_Group_QA_Team_Sales_etc(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(2000);
        System.out.println("USER ROLES -------------"+BusinessFunctions.getText(UserAccountPF.userRole()));
        BusinessFunctions.Assert_String_Case("1000\n" +
                "BMT\n" +
                "BONDDEALERSUP\n" +
                "BONDSALESNTRADE\n" +
                "Bond's DMA Sales\n" +
                "Bond's DMA Supervisor\n" +
                "Bond's Dealer\n" +
                "Bond's Manual Sales\n" +
                "Bond's Manual Supervisor\n" +
                "Bonds DMA+Manual Sales\n" +
                "DBS CC Checker\n" +
                "DBS RM / CC\n" +
                "DBS-BSM\n" +
                "DBS-BSU\n" +
                "DBS-WMO\n" +
                "FRT 1\n" +
                "God Like\n" +
                "IPAD\n" +
                "Investor Team\n" +
                "Laura auto all\n" +
                "PM Group\n" +
                "QA Team Role\n" +
                "RL BEA BMT\n" +
                "RL BEA IPAD\n" +
                "RL BEA RM\n" +
                "RLDBSBSM\n" +
                "RLDBSBSU\n" +
                "RLDBSRM\n" +
                "RLDBSWMO\n" +
                "RM (Relationship Manager)\n" +
                "Sales\n" +
                "Submit Only - No Self Approval\n" +
                "Supervisor\n" +
                "Test No Authorize\n" +
                "Test User 10\n" +
                "Test User 11\n" +
                "Test User 2\n" +
                "Test User 3\n" +
                "Test User 4\n" +
                "Test User 5\n" +
                "Testing Role Name\n" +
                "pmdemo",BusinessFunctions.getText(UserAccountPF.userRole()));

    }

    @When("^user \"([^\"]*)\"clicks on drop down field Dealer Group\"([^\"]*)\"$")
    public void user_clicks_on_drop_down_field_Dealer_Group(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.Click(UserAccountPF.dealerGroup());
    }

    @When("^System \"([^\"]*)\"should display dealer groups\\.\"([^\"]*)\"$")
    public void system_should_display_dealer_groups(String arg1, String arg2) {

        BusinessFunctions.Assert_String_Case("Group A\n"+
                        "Group B\n" +
                        "Group C",BusinessFunctions.getText(UserAccountPF.dealerGroup()));

    }

    @When("^user \"([^\"]*)\"clicks on User Group link\"([^\"]*)\"$")
    public void user_clicks_on_User_Group_link(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.Click(UserAccountPF.userGroup());
    }

    @When("^System \"([^\"]*)\"should display Mandatory fields and tables\\.\"([^\"]*)\"$")
    public void system_should_display_Mandatory_fields_and_tables(String arg1, String arg2) throws InterruptedException {


        Thread.sleep(4000);
        System.out.println("Validation"+BusinessFunctions.getText(UserAccountPF.userGroupTable()));
        BusinessFunctions.Assert_String_Case("User Group (Belonging)",BusinessFunctions.getText(UserAccountPF.userGroupTable()));

    }

    @When("^user \"([^\"]*)\"clicks on an entry in User Group belonging column and click on > button\"([^\"]*)\"$")
    public void user_clicks_on_an_entry_in_User_Group_belonging_column_and_click_on_button1(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.Click(UserAccountPF.userGroup());
        Thread.sleep(6000);
        BusinessFunctions.Click(UserAccountPF.belongingGroup());
        Thread.sleep(4000);
        BusinessFunctions.Click(UserAccountPF.removeGroupFromUser());



    }

    @When("^System \"([^\"]*)\"selected entry in User Group - belonging column is moved to User Group - not belonging column\"([^\"]*)\"$")
    public void system_selected_entry_in_User_Group_belonging_column_is_moved_to_User_Group_not_belonging_column(String arg1, String arg2) throws InterruptedException {


        Thread.sleep(4000);
       // BusinessFunctions.Assert_False_Boolean(UserAccountPF.belongingGroup().getText().contains("pmdemo"));
        BusinessFunctions.Assert_String_Case("Stephen Group (SGROUP)  \n" +
                "4",UserAccountPF.belongingGroup().getText());
        Thread.sleep(4000);
        BusinessFunctions.Click(UserAccountPF.reloadinGroup());


    }

    @When("^user \"([^\"]*)\"clicks on an entry in User Group belonging column and click on >> button\"([^\"]*)\"$")
    public void user_clicks_on_an_entry_in_User_Group_belonging_column_and_click_on_button2(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(4000);
        BusinessFunctions.Click(UserAccountPF.userGroup());
        Thread.sleep(6000);
        BusinessFunctions.Click(UserAccountPF.removeAllGroupsFromUser());
    }

    @When("^System \"([^\"]*)\"All entries in User Group - belonging column is moved to User Group - not belonging column\"([^\"]*)\"$")
    public void system_All_entries_in_User_Group_belonging_column_is_moved_to_User_Group_not_belonging_column(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.Assert_String_Case(" ",UserAccountPF.belongingGroup().getText());
        Thread.sleep(4000);
        BusinessFunctions.Click(UserAccountPF.reloadinGroup());
    }

    @When("^user \"([^\"]*)\"clicks on an entry in User Group belonging column and click on < button\"([^\"]*)\"$")
    public void user_clicks_on_an_entry_in_User_Group_belonging_column_and_click_on_button3(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(4000);
        BusinessFunctions.Click(UserAccountPF.userGroup());
        Thread.sleep(4000);
        BusinessFunctions.Click(UserAccountPF.notbelongingGroup());
        Thread.sleep(4000);
        BusinessFunctions.Click(UserAccountPF.addGrouptoUser());

    }

    @When("^System \"([^\"]*)\"selected entry in User Group - not belonging column is moved to User Group - belonging column\"([^\"]*)\"$")
    public void system_selected_entry_in_User_Group_not_belonging_column_is_moved_to_User_Group_belonging_column(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.Assert_String_Case("Ben Testing Group (BEN)  ",UserAccountPF.notbelongingGroup().getText());
        Thread.sleep(4000);
        BusinessFunctions.Click(UserAccountPF.reloadinGroup());

    }

    @When("^user \"([^\"]*)\"clicks on an entry in User Group belonging column and click on << button\"([^\"]*)\"$")
    public void user_clicks_on_an_entry_in_User_Group_belonging_column_and_click_on_button4(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(4000);
        BusinessFunctions.Click(UserAccountPF.userGroup());
        Thread.sleep(6000);
        BusinessFunctions.Click(UserAccountPF.addAllGroupsToUser());

    }

    @When("^System \"([^\"]*)\"All entries in User Group - not belonging column is moved to User Group - belonging column\"([^\"]*)\"$")
    public void system_All_entries_in_User_Group_not_belonging_column_is_moved_to_User_Group_belonging_column(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.Assert_String_Case(" ",UserAccountPF.notbelongingGroup().getText());
        Thread.sleep(4000);
        BusinessFunctions.Click(UserAccountPF.reloadinGroup());
        Thread.sleep(4000);

    }

    @When("^user \"([^\"]*)\"clicks on User Account link\"([^\"]*)\"$")
    public void user_clicks_on_User_Account_link(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(3000);
        BusinessFunctions.Click(UserAccountPF.userGroup());
        Thread.sleep(4000);
        BusinessFunctions.Click(UserAccountPF.backtoUserAccountPage());

    }

    @When("^System \"([^\"]*)\"should go back to User Account page\\.\"([^\"]*)\"$")
    public void system_should_go_back_to_User_Account_page(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.Assert_String_Case("User Account",UserAccountPF.userAccountPage().getText());

    }

    @When("^user \"([^\"]*)\"click on Trading Limit link \"([^\"]*)\"$")
    public void user_click_on_Trading_Limit_link(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.Click(UserAccountPF.tradingLimit());

    }

    @When("^System \"([^\"]*)\"should display Mandatory fields and tables in trading Limit page\\.\"([^\"]*)\"$")
    public void system_should_display_Mandatory_fields_and_tables_in_trading_Limit_page(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.Assert_String_Case("User Account Trading Limit",BusinessFunctions.getText(UserAccountPF.tradingLimitPage()));

    }

    @When("^In User Account page,click on plus icon\\.$")
    public void in_User_Account_page_click_on_plus_icon() throws InterruptedException {

        BusinessFunctions.selectFrame("mainIframe");
        Thread.sleep(2000);
        Thread.sleep(2000);
        BusinessFunctions.Click(UserAccountPF.addUser());

    }

    @Then("^Add User Account page is displayed with new user validation fields\\.$")
    public void add_User_Account_page_is_displayed_with_new_user_validation_fields()
    {
        BusinessFunctions.Assert_String_Case("Add User",BusinessFunctions.getText(UserAccountPF.addUserPage()));
    }


    @When("^user \"([^\"]*)\"input valid value for User ID, valid value for other fields and click Save button\"([^\"]*)\"$")
    public void user_input_valid_value_for_User_ID_valid_value_for_other_fields_and_click_Save_button(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(4000);
        BusinessFunctions.Click(UserAccountPF.userID());
        BusinessFunctions.Sendkeys(UserAccountPF.userID(),"ASCHAN22");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(UserAccountPF.userName(),"Alvin Chan22");
        Thread.sleep(4000);
        BusinessFunctions.Sendkeys(UserAccountPF.userPassword(),"12345");
        Thread.sleep(4000);
        BusinessFunctions.Sendkeys(UserAccountPF.confirmPassword(),"12345");
        Thread.sleep(2000);
        BusinessFunctions.Click(UserAccountPF.saveinGroup());

    }

    @When("^user \"([^\"]*)\"input blank value for User ID, valid value for other fields and click Save button\"([^\"]*)\"$")
    public void user_input_blank_value_for_User_ID_valid_value_for_other_fields_and_click_Save_button(String arg1, String arg2) throws InterruptedException {
        Thread.sleep(4000);
        BusinessFunctions.Click(UserAccountPF.userID());
        Thread.sleep(2000);
        BusinessFunctions.Click(UserAccountPF.saveinGroup());


    }

    @When("^user \"([^\"]*)\"input valid value for User Name, valid value for other fields and click Save button\"([^\"]*)\"$")
    public void user_input_valid_value_for_User_Name_valid_value_for_other_fields_and_click_Save_button(String arg1, String arg2) throws InterruptedException {
        Thread.sleep(4000);
        BusinessFunctions.Click(UserAccountPF.userID());
        BusinessFunctions.Sendkeys(UserAccountPF.userID(),"ASCHAN23");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(UserAccountPF.userName(),"Alvin Chan23");
        Thread.sleep(4000);
        BusinessFunctions.Sendkeys(UserAccountPF.userPassword(),"12345");
        Thread.sleep(4000);
        BusinessFunctions.Sendkeys(UserAccountPF.confirmPassword(),"12345");
        Thread.sleep(2000);
        BusinessFunctions.Click(UserAccountPF.saveinGroup());
    }

    @When("^user \"([^\"]*)\"input blank value for User Name, valid value for other fields and click Save button\"([^\"]*)\"$")
    public void user_input_blank_value_for_User_Name_valid_value_for_other_fields_and_click_Save_button(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(4000);
        BusinessFunctions.Click(UserAccountPF.userName());
        Thread.sleep(2000);
        BusinessFunctions.Click(UserAccountPF.saveinGroup());

    }



    /*@When("^System \"([^\"]*)\"should display \"([^\"]*)\" in add user page\\.\"([^\"]*)\"$")
    public void system_should_display_in_add_user_page(String arg1, String arg2, String arg3) throws InterruptedException {

        Thread.sleep(4000);
        BusinessFunctions.Assert_String_Case("\"User ID\" is required.\n" +
                "\"User Name\" is required.\n" +
                "\"User Password\" is required.\n" +
                "\"Confirm Password\" is required.",BusinessFunctions.getText(ValidationPF.message()));
        BusinessFunctions.Click(ValidationPF.okButton());

    }*/

    @When("^System \"([^\"]*)\"should display \"([^\"]*)\" in addUser Page for ID\\.\"([^\"]*)\"$")
    public void system_should_display_in_addUser_Page_for_ID(String arg1, String arg2, String arg3) throws InterruptedException {

        Thread.sleep(4000);
        BusinessFunctions.Assert_String_Case("\"User ID\" is required.\n" +
                "\"User Name\" is required.\n" +
                "\"User Password\" is required.\n" +
                "\"Confirm Password\" is required.",BusinessFunctions.getText(ValidationPF.message()));
        BusinessFunctions.Click(ValidationPF.okButton());
    }

    @When("^System \"([^\"]*)\"should display \"([^\"]*)\" in addUser Page for Name\\.\"([^\"]*)\"$")
    public void system_should_display_in_addUser_Page_for_Name(String arg1, String arg2, String arg3) throws InterruptedException {

        Thread.sleep(4000);
        BusinessFunctions.Assert_String_Case("\"User ID\" is required.\n" +
                "\"User Name\" is required.\n" +
                "\"User Password\" is required.\n" +
                "\"Confirm Password\" is required.",BusinessFunctions.getText(ValidationPF.message()));
        BusinessFunctions.Click(ValidationPF.okButton());

    }


    @When("^user \"([^\"]*)\"input valid value for User Password, same value for Confirm Password\"([^\"]*)\"$")
    public void user_input_valid_value_for_User_Password_same_value_for_Confirm_Password(String arg1, String arg2) throws InterruptedException {
        Thread.sleep(4000);
        BusinessFunctions.Click(UserAccountPF.userID());
        BusinessFunctions.Sendkeys(UserAccountPF.userID(),"ASCHAN24213");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(UserAccountPF.userName(),"Alvin Chan24213");
        Thread.sleep(4000);
        BusinessFunctions.Sendkeys(UserAccountPF.userPassword(),"12345");
        Thread.sleep(4000);
        BusinessFunctions.Sendkeys(UserAccountPF.confirmPassword(),"12345");
        Thread.sleep(2000);
        BusinessFunctions.Click(UserAccountPF.saveinGroup());

    }

    @When("^user \"([^\"]*)\"input blank value for Confirm Password\"([^\"]*)\"$")
    public void user_input_blank_value_for_Confirm_Password(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(4000);
        BusinessFunctions.Click(UserAccountPF.userID());
        BusinessFunctions.Sendkeys(UserAccountPF.userID(),"ASCHAN24");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(UserAccountPF.userName(),"Alvin Chan24");
        Thread.sleep(4000);
        BusinessFunctions.Sendkeys(UserAccountPF.userPassword(),"12345");
        Thread.sleep(4000);
        BusinessFunctions.Sendkeys(UserAccountPF.confirmPassword(),"");
        Thread.sleep(2000);
        BusinessFunctions.Click(UserAccountPF.saveinGroup());

    }

    @When("^System \"([^\"]*)\"should display \"([^\"]*)\" in addUser Page for Confirm Password\\.\"([^\"]*)\"$")
    public void system_should_display_in_addUser_Page_for_Confirm_Password(String arg1, String arg2, String arg3) throws InterruptedException {

        Thread.sleep(4000);
        BusinessFunctions.Assert_String_Case("\"Confirm Password\" is required.",BusinessFunctions.getText(ValidationPF.message()));
        BusinessFunctions.Click(ValidationPF.okButton());

    }

    @When("^user \"([^\"]*)\"input different value for User Password and confirm password\"([^\"]*)\"$")
    public void user_input_different_value_for_User_Password_and_confirm_password(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(4000);
        BusinessFunctions.Click(UserAccountPF.userID());
        BusinessFunctions.Sendkeys(UserAccountPF.userID(),"ASCHAN24");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(UserAccountPF.userName(),"Alvin Chan24");
        Thread.sleep(4000);
        BusinessFunctions.Sendkeys(UserAccountPF.userPassword(),"12345");
        Thread.sleep(4000);
        BusinessFunctions.Sendkeys(UserAccountPF.confirmPassword(),"23123");
        Thread.sleep(2000);
        BusinessFunctions.Click(UserAccountPF.saveinGroup());
    }

    @When("^System \"([^\"]*)\"should display \"([^\"]*)\" does not match \"([^\"]*)\"\\.\"([^\"]*)\"$")
    public void system_should_display_does_not_match(String arg1, String arg2, String arg3, String arg4) throws InterruptedException {

        Thread.sleep(4000);
        BusinessFunctions.Assert_String_Case("\"User Password\" does not match \"Confirm Password\".",BusinessFunctions.getText(ValidationPF.message()));
        BusinessFunctions.Click(ValidationPF.okButton());

    }


}
