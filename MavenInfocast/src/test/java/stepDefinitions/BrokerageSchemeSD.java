package stepDefinitions;

import PageObjectModel.BrokerageSchemePF;
import PageObjectModel.UserAccountPF;
import PageObjectModel.ValidationPF;
import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.lexer.Th;
import net.rcarz.jiraclient.User;
import utility.BusinessFunctions;

import java.sql.BatchUpdateException;

public class BrokerageSchemeSD {

    @Then("^Click on the System -> Brokerage Scheme page\\.$")
    public void click_on_the_System_Brokerage_Scheme_page() throws InterruptedException {

        Thread.sleep(4000);

        BusinessFunctions.Click(ValidationPF.system());
        Thread.sleep(2000);
        BusinessFunctions.Click(BrokerageSchemePF.brokerageScheme());


    }

    @When("^In Brokerage Scheme page, comes with search and result tabs\\.$")
    public void in_Brokerage_Scheme_page_comes_with_search_and_result_tabs() throws InterruptedException {
        //BusinessFunctions.selectFrame("mainIframe");

        BusinessFunctions.selectFrame("mainIframe");
        Thread.sleep(3000);
        //BusinessFunctions.Click(BrokerageSchemePF.searchBkgSchCode());
        Thread.sleep(4000);
        BusinessFunctions.Assert_String_Case("Search", BusinessFunctions.getText(BrokerageSchemePF.searchTab()));
        Thread.sleep(4000);
       // BusinessFunctions.Click(BrokerageSchemePF.resultTab());
        Thread.sleep(3000);
        BusinessFunctions.Assert_String_Case("Result",BusinessFunctions.getText(BrokerageSchemePF.resultTab()));
    }

    @Then("^verify fields in search tab$")
    public void verify_fields_in_search_tab() throws InterruptedException {
        Thread.sleep(2000);
        BusinessFunctions.Assert_String_Case("Scheme Code", BusinessFunctions.getText(BrokerageSchemePF.bkgSchCode()));
        BusinessFunctions.Assert_String_Case("Scheme Name",BusinessFunctions.getText(BrokerageSchemePF.bkgSchName()));

    }

    @Then("^verify fields in result tab\\.$")
    public void verify_fields_in_result_tab() throws InterruptedException {
        BusinessFunctions.Click(BrokerageSchemePF.resultTab());
        Thread.sleep(2000);
        BusinessFunctions.Assert_String_Case("Code",BusinessFunctions.getText(BrokerageSchemePF.codeInResultTab()));
    }

    @When("^In Brokerage Scheme page, enter random values in Search fields\\.$")
    public void in_Brokerage_Scheme_page_enter_random_values_in_Search_fields() throws InterruptedException {

        BusinessFunctions.selectFrame("mainIframe");
        Thread.sleep(3000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.searchBkgSchCode(),"1234");
        Thread.sleep(3000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.searchBkgSchName(),"1212");

    }

    @Then("^click on clear field\\.$")
    public void click_on_clear_field() throws InterruptedException {
        Thread.sleep(2000);
        BusinessFunctions.Click(BrokerageSchemePF.clear());

    }

    @Then("^System should retrive back previous values\\.$")
    public void system_should_retrive_back_previous_values() {

        BusinessFunctions.Assert_String_Case("",BusinessFunctions.getText(BrokerageSchemePF.searchBkgSchCode()));
        BusinessFunctions.Assert_String_Case("",BusinessFunctions.getText(BrokerageSchemePF.searchBkgSchName()));

    }

    @When("^In Brokerage Scheme page, enter valid User ID and click on Search button\\.$")
    public void in_Brokerage_Scheme_page_enter_valid_User_ID_and_click_on_Search_button() throws InterruptedException {

        BusinessFunctions.selectFrame("mainIframe");
        Thread.sleep(4000);
        BusinessFunctions.Click(BrokerageSchemePF.searchTab());
        BusinessFunctions.Sendkeys(BrokerageSchemePF.searchBkgSchCode(),"A0001");
        Thread.sleep(3000);
        BusinessFunctions.Click(BrokerageSchemePF.search());

    }


    @When("^In Brokerage Scheme page,User clicks on the first entry\\.$")
    public void in_Brokerage_Scheme_page_User_clicks_on_the_first_entry() throws InterruptedException {

        Thread.sleep(4000);
        BusinessFunctions.Click(BrokerageSchemePF.firstEntry());
        Thread.sleep(2000);

    }


    @When("^user \"([^\"]*)\"modify Scheme Name and click Reload button\"([^\"]*)\"$")
    public void user_modify_Scheme_Name_and_click_Reload_button(String arg1, String arg2) throws InterruptedException {

        BusinessFunctions.clearField(BrokerageSchemePF.userBkgSchName());
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchName(),"cajcjede");
        Thread.sleep(2000);
        BusinessFunctions.Click(BrokerageSchemePF.reload());


    }

    @When("^System \"([^\"]*)\"should retrieve back the original data in Scheme Name\\.\"([^\"]*)\"$")
    public void system_System_should_retrieve_back_the_original_data_in_Scheme_Name(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(5000);
        BusinessFunctions.Assert_String_Case("Prime rate 5.25% 123",BusinessFunctions.getText(BrokerageSchemePF.userBkgSchName()));
        Thread.sleep(5000);

    }

    @When("^user \"([^\"]*)\"click Delete button \"([^\"]*)\"$")
    public void user_click_Delete_button(String arg1, String arg2) throws InterruptedException {

        BusinessFunctions.Click(BrokerageSchemePF.delete());
        Thread.sleep(2000);

    }

    @When("^System \"([^\"]*)\"displays prompt window for Yes/No confirmation\"([^\"]*)\"$")
    public void system_displays_prompt_window_for_Yes_No_confirmation(String arg1, String arg2) throws InterruptedException {

        BusinessFunctions.Assert_String_Case("You cannot recover a deleted record, continue?",BusinessFunctions.getText(ValidationPF.message()));
        Thread.sleep(2000);
    }

    @When("^user \"([^\"]*)\"input valid Scheme Name, Rate % as (\\d+) and click Save button\"([^\"]*)\"$")
    public void user_input_valid_Scheme_Name_Rate_as_and_click_Save_button(String arg1, int arg2, String arg3) throws InterruptedException {

        BusinessFunctions.clearField(BrokerageSchemePF.userBkgSchName());
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchName(),"Prime rate 5.25% 123");
        Thread.sleep(2000);
        BusinessFunctions.clearField(BrokerageSchemePF.userBkgSchRate());
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchRate(),"1");
        Thread.sleep(2000);
        BusinessFunctions.Click(BrokerageSchemePF.save());

    }

    @When("^user \"([^\"]*)\"input blank value for Scheme Name and Rate % as (\\d+) and click Save button\"([^\"]*)\"$")
    public void user_input_blank_value_for_Scheme_Name_and_Rate_as_and_click_Save_button(String arg1, int arg2, String arg3) throws InterruptedException {

        BusinessFunctions.clearField(BrokerageSchemePF.userBkgSchName());
        Thread.sleep(2000);

        Thread.sleep(2000);
        BusinessFunctions.Click(BrokerageSchemePF.save());


    }

    @When("^System \"([^\"]*)\"should display \"([^\"]*)\" in Brokerage Scheme page\\.\"([^\"]*)\"$")
    public void system_should_display_in_Brokerage_Scheme_page(String arg1, String arg2, String arg3) throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.Assert_String_Case("\"Scheme Name\" is required.",BusinessFunctions.getText(ValidationPF.message()));
        Thread.sleep(2000);


    }

    @When("^user \"([^\"]*)\"input valid Scheme Name, input blank value for Rate % and Min Amount and click Save button\"([^\"]*)\"$")
    public void user_input_valid_Scheme_Name_input_blank_value_for_Rate_and_Min_Amount_and_click_Save_button(String arg1, String arg2) throws InterruptedException {

        BusinessFunctions.clearField(BrokerageSchemePF.userBkgSchName());
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchName(),"Prime rate 5.25% 123");
        Thread.sleep(2000);
        BusinessFunctions.clearField(BrokerageSchemePF.userBkgSchRate());
        Thread.sleep(2000);
        BusinessFunctions.clearField(BrokerageSchemePF.userBkgSchAmount());
        Thread.sleep(2000);
        BusinessFunctions.Click(BrokerageSchemePF.save());

    }

    @When("^System \"([^\"]*)\"should display Either Rate % or Minimum Brokerage Amount is required\\.\"([^\"]*)\"$")
    public void system_should_display_Either_Rate_or_Minimum_Brokerage_Amount_is_required(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.Assert_String_Case("Either \"Rate (%)\" or \"Minimum Brokerage Amount\" is required.",BusinessFunctions.getText(ValidationPF.message()));
        Thread.sleep(2000);

    }

    @When("^user \"([^\"]*)\"input Rate % between (\\d+)\\.(\\d+) to (\\d+)\\.(\\d+) and click Save button\"([^\"]*)\"$")
    public void user_input_Rate_between_to_and_click_Save_button(String arg1, int arg2, int arg3, int arg4, int arg5, String arg6) throws InterruptedException {


        Thread.sleep(3000);
        BusinessFunctions.clearField(BrokerageSchemePF.userBkgSchRate());
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchRate(),"1");
        Thread.sleep(2000);
        BusinessFunctions.Click(BrokerageSchemePF.save());
        Thread.sleep(2000);


    }

    @When("^user \"([^\"]*)\" input Rate % outside range of (\\d+)\\.(\\d+) to (\\d+)\\.(\\d+) and click Save button\"([^\"]*)\"$")
    public void user_input_Rate_outside_range_of_to_and_click_Save_button(String arg1, int arg2, int arg3, int arg4, int arg5, String arg6) throws InterruptedException {


        BusinessFunctions.clearField(BrokerageSchemePF.userBkgSchRate());
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchRate(),"101");
        Thread.sleep(2000);
        BusinessFunctions.Click(BrokerageSchemePF.save());
        Thread.sleep(2000);

    }

    @When("^System \"([^\"]*)\"should display \"([^\"]*)\" in Brokerage Scheme page for rate\\.\"([^\"]*)\"$")
    public void system_should_display_in_Brokerage_Scheme_page_for_rate(String arg1, String arg2, String arg3) throws InterruptedException {



        Thread.sleep(2000);
        BusinessFunctions.Assert_String_Case("\"Rate (%)\" must be between 0.00001 to 99.99999.",BusinessFunctions.getText(ValidationPF.message()));
        Thread.sleep(2000);
    }



    @When("^user \"([^\"]*)\" input valid Min Amount and click Save button\"([^\"]*)\"$")
    public void user_input_valid_Min_Amount_and_click_Save_button(String arg1, String arg2) throws InterruptedException {

        BusinessFunctions.clearField(BrokerageSchemePF.userBkgSchAmount());
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchAmount(),"1324567");
        Thread.sleep(2000);
        BusinessFunctions.Click(BrokerageSchemePF.save());
        Thread.sleep(2000);

    }

    @When("^user \"([^\"]*)\" input invalid Min Amount \"([^\"]*)\"$")
    public void user_input_invalid_Min_Amount(String arg1, String arg2) throws InterruptedException {

        BusinessFunctions.clearField(BrokerageSchemePF.userBkgSchAmount());
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchAmount(),".");
        Thread.sleep(2000);
        BusinessFunctions.Click(BrokerageSchemePF.save());
        Thread.sleep(2000);
    }

    @When("^System \"([^\"]*)\"should display Invalid input values for Min Amount is not entered in the field\\.\"([^\"]*)\"$")
    public void system_should_display_Invalid_input_values_for_Min_Amount_is_not_entered_in_the_field(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.Assert_String_Case("\"Minimum Brokerage Amount\" is not a valid number.",BusinessFunctions.getText(ValidationPF.message()));
        Thread.sleep(2000);

    }

    @When("^In Brokerage Scheme Maintenance page, click on plus icon\\.$")
    public void in_Brokerage_Scheme_Maintenance_page_click_on_plus_icon() throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.selectFrame("mainIframe");
        Thread.sleep(4000);
        BusinessFunctions.Click(BrokerageSchemePF.searchTab());
        BusinessFunctions.Click(UserAccountPF.addUser());

    }

    @Then("^verify fields in Add Brokerage Scheme page\\.$")
    public void verify_fields_in_Add_Brokerage_Scheme_page() throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.Assert_String_Case("Add Brokerage Scheme",BusinessFunctions.getText(BrokerageSchemePF.addBrokerageSchemePage()));
    }


    @When("^user \"([^\"]*)\" input valid values for Scheme Code, Scheme Name, Rate %, Min Amount and click Save\"([^\"]*)\"$")
    public void user_input_valid_values_for_Scheme_Code_Scheme_Name_Rate_Min_Amount_and_click_Save(String arg1, String arg2) throws InterruptedException {

        BusinessFunctions.Sendkeys(BrokerageSchemePF.userbkgSchCode(),"A0011");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchName(),"Prime rate 5.25% 12345");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchRate(),"1");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchAmount(),"1234567");
        Thread.sleep(2000);
        BusinessFunctions.Click(BrokerageSchemePF.save());
    }

    @When("^user \"([^\"]*)\" modify Margin Class Name and click Reload button \"([^\"]*)\"$")
    public void user_modify_Margin_Class_Name_and_click_Reload_button(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchName(),"Prime rate 5.25% 12345");
        Thread.sleep(2000);
        BusinessFunctions.Click(BrokerageSchemePF.reload());


    }

    @When("^System \"([^\"]*)\"should retrieve back the original data in add brokerage page\\.\"([^\"]*)\"$")
    public void system_should_retrieve_back_the_original_data_in_add_brokerage_page(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.Assert_String_Case("",BusinessFunctions.getText(BrokerageSchemePF.userBkgSchName()));

    }

    @When("^user \"([^\"]*)\" input valid values for Scheme Code, Scheme Name, Rate %, Min Amount and click on Save\"([^\"]*)\"$")
    public void user_input_valid_values_for_Scheme_Code_Scheme_Name_Rate_Min_Amount_and_click_on_Save(String arg1, String arg2) throws InterruptedException {

        BusinessFunctions.Sendkeys(BrokerageSchemePF.userbkgSchCode(),"A0012");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchName(),"Prime rate 5.25% 123456");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchRate(),"1");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchAmount(),"1234567");
        Thread.sleep(2000);
        BusinessFunctions.Click(BrokerageSchemePF.save());

    }

    @When("^user \"([^\"]*)\" input blank value for Scheme Code, valid Scheme Name and Rate % as (\\d+) and click Save button\"([^\"]*)\"$")
    public void user_input_blank_value_for_Scheme_Code_valid_Scheme_Name_and_Rate_as_and_click_Save_button(String arg1, int arg2, String arg3) throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchName(),"Prime rate 5.25% 123456");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchRate(),"1");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchAmount(),"1234567");
        Thread.sleep(2000);
        BusinessFunctions.Click(BrokerageSchemePF.save());
    }

    @When("^System \"([^\"]*)\"should display \"([^\"]*)\" in Brokerage Scheme page for Code\\.\"([^\"]*)\"$")
    public void system_should_display_in_Brokerage_Scheme_page_for_Code(String arg1, String arg2, String arg3) throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.Assert_String_Case("\"Scheme Code\" is required.",BusinessFunctions.getText(ValidationPF.message()));


    }

    @When("^user \"([^\"]*)\" input valid Scheme Code, valid Scheme Name,  input blank value for Rate % and Min Amount and click Save button\"([^\"]*)\"$")
    public void user_input_valid_Scheme_Code_valid_Scheme_Name_input_blank_value_for_Rate_and_Min_Amount_and_click_Save_button(String arg1, String arg2) throws InterruptedException {

        BusinessFunctions.Sendkeys(BrokerageSchemePF.userbkgSchCode(),"A0012");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchName(),"Prime rate 5.25% 123456");
        Thread.sleep(2000);
        BusinessFunctions.Click(BrokerageSchemePF.save());

    }

    @When("^user \"([^\"]*)\" input Rate % between (\\d+)\\.(\\d+) to (\\d+)\\.(\\d+) ad valid values for other fields and click Save button\"([^\"]*)\"$")
    public void user_input_Rate_between_to_ad_valid_values_for_other_fields_and_click_Save_button(String arg1, int arg2, int arg3, int arg4, int arg5, String arg6) throws InterruptedException {

        BusinessFunctions.Sendkeys(BrokerageSchemePF.userbkgSchCode(),"A0013");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchName(),"Prime rate 5.25% 123456");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchRate(),"1");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchAmount(),"1234567");
        Thread.sleep(2000);
        BusinessFunctions.Click(BrokerageSchemePF.save());
    }

    @When("^user \"([^\"]*)\" input Rate % outside range of (\\d+)\\.(\\d+) to (\\d+)\\.(\\d+) and valid values for other fields and click Save\"([^\"]*)\"$")
    public void user_input_Rate_outside_range_of_to_and_valid_values_for_other_fields_and_click_Save(String arg1, int arg2, int arg3, int arg4, int arg5, String arg6) throws InterruptedException {

        BusinessFunctions.Sendkeys(BrokerageSchemePF.userbkgSchCode(),"A0012");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchName(),"Prime rate 5.25% 123456");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchRate(),"101");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchAmount(),"1234567");
        Thread.sleep(2000);
        BusinessFunctions.Click(BrokerageSchemePF.save());

    }

    @When("^user \"([^\"]*)\" input valid Min Amount ad valid values for other fields and click Save\"([^\"]*)\"$")
    public void user_input_valid_Min_Amount_ad_valid_values_for_other_fields_and_click_Save(String arg1, String arg2) throws InterruptedException {

        BusinessFunctions.Sendkeys(BrokerageSchemePF.userbkgSchCode(),"A0014");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchName(),"Prime rate 5.25% 123456");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchRate(),"1");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchAmount(),"1234567");
        Thread.sleep(2000);
        BusinessFunctions.Click(BrokerageSchemePF.save());

    }

    @When("^user \"([^\"]*)\" input invalid Min Amount ad valid values for other fields\"([^\"]*)\"$")
    public void user_input_invalid_Min_Amount_ad_valid_values_for_other_fields(String arg1, String arg2) throws InterruptedException {

        BusinessFunctions.Sendkeys(BrokerageSchemePF.userbkgSchCode(),"A0014");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchName(),"Prime rate 5.25% 123456");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchRate(),"1");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchAmount(),".");
        Thread.sleep(2000);
        BusinessFunctions.Click(BrokerageSchemePF.save());
    }

    @When("^user \"([^\"]*)\" input blank value for Min Amount, Rate % and valid values for other fields\"([^\"]*)\"$")
    public void user_input_blank_value_for_Min_Amount_Rate_and_valid_values_for_other_fields(String arg1, String arg2) throws InterruptedException {

        BusinessFunctions.Sendkeys(BrokerageSchemePF.userbkgSchCode(),"A0014");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchName(),"Prime rate 5.25% 123456");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchRate(),"");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchAmount(),"");
        Thread.sleep(2000);
        BusinessFunctions.Click(BrokerageSchemePF.save());
    }

    @When("^user \"([^\"]*)\" input blank value for Min Amount, Rate % and valid values for other fields and click reload button\"([^\"]*)\"$")
    public void user_input_blank_value_for_Min_Amount_Rate_and_valid_values_for_other_fields_and_click_reload_button(String arg1, String arg2) throws InterruptedException {
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userbkgSchCode(),"A0014");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchName(),"Prime rate 5.25% 123456");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchRate(),"");
        Thread.sleep(2000);
        BusinessFunctions.Sendkeys(BrokerageSchemePF.userBkgSchAmount(),"");
        Thread.sleep(2000);
        BusinessFunctions.Click(BrokerageSchemePF.reload());


    }



    @When("^user \"([^\"]*)\"empty all in add brokerage page\\.\"([^\"]*)\"$")
    public void user_empty_all_in_add_brokerage_page(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.Click(BrokerageSchemePF.save());


    }

    @When("^System \"([^\"]*)\"should retrieve back the original data in addbrokeragePage\\.\"([^\"]*)\"$")
    public void system_should_retrieve_back_the_original_data_in_addbrokeragePage(String arg1, String arg2) throws InterruptedException {

        Thread.sleep(4000);
        BusinessFunctions.Assert_String_Case("\"Scheme Code\" is required.\n" +
                "\"Scheme Name\" is required.\n" +
                "Either \"Rate (%)\" or \"Minimum Brokerage Amount\" is required.",BusinessFunctions.getText(ValidationPF.message()));

    }


}
