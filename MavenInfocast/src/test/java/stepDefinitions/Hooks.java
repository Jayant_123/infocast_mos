package stepDefinitions;

import java.io.IOException;

import PageObjectModel.ValidationPF;
import org.openqa.selenium.support.ui.WebDriverWait;
import utility.BusinessFunctions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import testlink.api.java.client.TestLinkAPIClient;
import testlink.api.java.client.TestLinkAPIHelper;
import testlink.api.java.client.TestLinkAPIResults;
import utility.TestlinkIntegration;


public class Hooks {

    public static String testlinkURL = System.getProperty("testlink_url");


	protected static WebDriver driver = null;
	public static WebDriverWait waitdriver;
	public static String browser = System.getProperty("browser_name");
    public static String testid;
    TestLinkAPIHelper help = new TestLinkAPIHelper();
    TestLinkAPIClient testlink = new TestLinkAPIClient("daa88d6c650efc6013f88fbcbd64fc77",testlinkURL);

    @Before
	public static WebDriver initDriver() {
		System.out.println(browser);
		if (driver == null) {
			if ("FF".equalsIgnoreCase(browser)) {
				System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\lib\\driver32\\geckodriver.exe");
				DesiredCapabilities dc = DesiredCapabilities.firefox();
				dc.setCapability("marionette", false);
				dc.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				dc.setCapability("disable-restore-session-state",true);
				driver = new FirefoxDriver(dc);
				waitdriver = new WebDriverWait(driver,60);

			} else if ("IE".equalsIgnoreCase(browser)) {

				System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + "\\lib\\IEDriverServer.exe");
				driver = new InternetExplorerDriver();
				waitdriver = new WebDriverWait(driver,60);
			} else if ("Chrome".equalsIgnoreCase(browser)) {

				System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\lib\\chromeP\\chromedriver.exe");
				driver = new ChromeDriver();
				waitdriver = new WebDriverWait(driver,60);

			} else {
				// default Chrome
				System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\lib\\chromeP\\chromedriver.exe");
				driver = new ChromeDriver();
				waitdriver = new WebDriverWait(driver,60);

			}
		}

		return driver;
	}


	@After
    public void afterScenario(Scenario scenario) throws IOException, TestLinkAPIException, testlink.api.java.client.TestLinkAPIException, InterruptedException {

		BusinessFunctions.parentFrame();
		Thread.sleep(2000);
		BusinessFunctions.Click(ValidationPF.logout());

        System.out.println(testid);
		if(scenario.isFailed())
			{
				BusinessFunctions.captureScreenshot();
				System.out.println("Failed");
				TestlinkIntegration.updateResults(testid, "FAIL" ,TestLinkAPIResults.TEST_FAILED);

			}
			else
			{
			
				System.out.println("Passed");
				TestlinkIntegration.updateResults(testid, "Pass" ,TestLinkAPIResults.TEST_PASSED);
		
			}

 }
}
