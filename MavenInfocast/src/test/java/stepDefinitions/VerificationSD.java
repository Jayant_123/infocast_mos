package stepDefinitions;

import PageObjectModel.ValidationPF;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utility.BusinessFunctions;

public class VerificationSD {





    @When("^User performs some modifications and click on Save button\\.$")
    public void user_performs_some_modifications_and_click_on_Save_button(){

        BusinessFunctions.clearField(ValidationPF.passwordHistory());
        BusinessFunctions.Sendkeys(ValidationPF.passwordHistory(),"0");
        BusinessFunctions.Click(ValidationPF.passwordSave());

    }

    @When("^User performs some modifications and click on Reload button without saving the data\\.$")
    public void user_performs_some_modifications_and_click_on_Reload_button_without_saving_the_data() throws InterruptedException {

        BusinessFunctions.clearField(ValidationPF.passExpiryButton());
        BusinessFunctions.Sendkeys(ValidationPF.passExpiryButton(),"181");
        BusinessFunctions.Click(ValidationPF.reload());

    }

    @Then("^System should able to undo the changes and retrieve back the original values\\.$")
    public void system_should_able_to_undo_the_changes_and_retrieve_back_the_original_values() throws InterruptedException {

        Thread.sleep(4000);
        System.out.println("Value -------"+ValidationPF.passExpiryButton().getTagName());
        BusinessFunctions.Assert_String_Case("passwordExpiryDay",ValidationPF.passExpiryButton().getAttribute("name"));

    }

    @When("^User clicks on the Company link in password policy maintenance page\\.$")
    public void user_clicks_on_the_Company_link_in_password_policy_maintenance_page() {

        BusinessFunctions.Click(ValidationPF.backtoCompanyPage());
    }

    @Then("^System should navigate back to company Maintenance page\\.$")
    public void system_should_navigate_back_to_company_Maintenance_page() {

        BusinessFunctions.Assert_String_Case("Company Maintenance",BusinessFunctions.getText(ValidationPF.companyMaintainancePage()));
    }

    @Then("^The password policy maintenance page should be displayed with all the validation fields\\.$")
    public void the_password_policy_maintenance_page_should_be_displayed_with_all_the_validation_fields() {
System.out.println("Password History----"+ValidationPF.passwordHistory().getAttribute("name"));
        BusinessFunctions.Assert_String_Case("passwordHistory",ValidationPF.passwordHistory().getAttribute("name"));

    }

    @Then("^System should save the data and display \"([^\"]*)\" message\\.$")
    public void system_should_save_the_data_and_display_message(String arg1) throws InterruptedException {

        Thread.sleep(2000);
        BusinessFunctions.Assert_String_Case("Saved.", BusinessFunctions.getText(ValidationPF.message()));

    }
}
